#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <windows.h>

#include "util/so_stdio.h"

#define BUF_SIZE 4096
#define NO_OP 0
#define READ_OP 1
#define WRITE_OP 2

#define MODE_READ 1
#define MODE_READ_PLUS 2
#define MODE_WRITE 3
#define MODE_WRITE_PLUS 4
#define MODE_APPEND 5
#define MODE_APPEND_PLUS 6

/* Pastram in structura SO_FILE un file_descriptor pentru fisierul deschis */
struct _so_file {
	HANDLE hFile;
	unsigned char *buffer;
	/* Tinem minte cate caractere avem in momentul de fata in buffer */
	int read_limit;
	/* Tinem minte la ce caracter am ajuns cu citirea in buffer */
	int read_index;
	int write_index;
	/* Tinem minte ultima operatie efectuata pe fisier (READ / WRITE) */
	int last_op;
	int error;
	DWORD file_cursor;
	int mode;

	int is_popen;
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
};

int parse_mode(const char *mode)
{
	if (strcmp(mode, "r") == 0)
		return MODE_READ;
	if (strcmp(mode, "r+") == 0)
		return MODE_READ_PLUS ;
	if (strcmp(mode, "w") == 0)
		return MODE_WRITE;
	if (strcmp(mode, "w+") == 0)
		return MODE_WRITE_PLUS;
	if (strcmp(mode, "a") == 0)
		return MODE_APPEND;
	if (strcmp(mode, "a+") == 0)
		return MODE_APPEND_PLUS;

	return -1;
}

DWORD parse_raed_write_mode(int mode)
{
	switch (mode) {
	case MODE_READ:
		return GENERIC_READ;
	case MODE_WRITE:
	case MODE_APPEND:
		return GENERIC_WRITE;
	case MODE_READ_PLUS:
	case MODE_WRITE_PLUS:
	case MODE_APPEND_PLUS:
		return GENERIC_READ | GENERIC_WRITE;
	default:
		return (DWORD) -1;
	};
}

DWORD parse_creation_mode(int mode)
{
	switch (mode) {
	case MODE_READ:
	case MODE_READ_PLUS:
		return OPEN_EXISTING;
	case MODE_WRITE:
	case MODE_WRITE_PLUS:
		return CREATE_ALWAYS;
	case MODE_APPEND:
	case MODE_APPEND_PLUS:
		return OPEN_ALWAYS;
	default:
		return (DWORD) -1;
	};
}

SO_FILE *so_fopen(const char *pathname, const char *mode)
{
	SO_FILE *stream;

	/* Alocam o structura SO_FILE */
	stream = malloc(sizeof(SO_FILE));
	if (stream == NULL)
		return NULL;
	stream->buffer =
		(unsigned char *) calloc(BUF_SIZE, sizeof(unsigned char));
	if (stream->buffer == NULL)
		return NULL;

	stream->read_limit = -BUF_SIZE;
	stream->read_index = -1;
	stream->write_index = -1;
	/* Initial nu s-a efectuat nicio operatie pe fisier */
	stream->last_op = NO_OP;
	stream->error = 0;
	stream->file_cursor = 0;
	stream->mode = parse_mode(mode);
	stream->is_popen = 0;

	stream->hFile = CreateFile(
		pathname,
		parse_raed_write_mode(stream->mode),
		FILE_SHARE_READ,
		NULL,	/* no security attributes */
		parse_creation_mode(stream->mode),
		FILE_ATTRIBUTE_NORMAL,
		NULL	/* no pattern */
	);

	/* Daca nu am reusit, atunci iesim */
	if (stream->hFile == INVALID_HANDLE_VALUE) {
		free(stream->buffer);
		free(stream);

		return NULL;
	}

	return stream;
}

int so_fclose(SO_FILE *stream)
{
	int rc;
	BOOL bRet;

	if (stream->last_op == WRITE_OP)
		if (so_fflush(stream) == SO_EOF) {
			stream->error = SO_EOF;
			free(stream->buffer);
			free(stream);

			return SO_EOF;
		}

	/* close file */
	bRet = CloseHandle(stream->hFile);
	if (bRet == FALSE)
		return SO_EOF;

	free(stream->buffer);
	free(stream);

	return 0;
}

int read_in_buffer(SO_FILE *stream)
{
	int read_bytes;
	BOOL bRet;

	/* Setam buffer-ul pe 0 inainte de a-l popula */
	memset(stream->buffer, 0, BUF_SIZE);

	/* Citim datele din fisier in buffer */
	bRet = ReadFile(
		stream->hFile,
		stream->buffer,
		BUF_SIZE,
		&read_bytes,
		NULL
	);
	if (bRet == FALSE) {
		stream->read_limit = 0;
		stream->error = SO_EOF;
		return SO_EOF;
	}

	if (read_bytes < BUF_SIZE)
		stream->buffer[read_bytes] = SO_EOF;

	/* Retinem cate caractere au fost citite in buffer */
	stream->read_limit = read_bytes;
	/* Resetam cursorul bufferului pentru datele noi */
	stream->read_index = 0;
	stream->write_index = -1;

	return read_bytes;
}

int so_fgetc(SO_FILE *stream)
{
	int rc = 0;
	unsigned char c;

	/* Daca bufferul este gol sau cursorul de citire al bufferului */
	/* a depasit ultimul caracter citit, repopulam buffer-ul */
	if (
			stream->read_index == -BUF_SIZE ||
			stream->read_limit - stream->read_index <= 0
			) {
		rc = read_in_buffer(stream);
		/* Ne asiguram ca nu am avut probleme la citire */
		if (so_feof(stream) == 1)
			return SO_EOF;
	}

	c = stream->buffer[stream->read_index];
	stream->read_index++;
	stream->file_cursor++;
	stream->last_op = READ_OP;

	return (int) c;
}


int so_fflush(SO_FILE *stream)
{
	int written_bytes;
	BOOL bRet;
	DWORD old_real_file_cursor;

	if (stream->mode == MODE_APPEND || stream->mode == MODE_APPEND_PLUS) {
		/* Salvam vechea pozitie la care ne aflam in fisier */
		old_real_file_cursor = SetFilePointer(
			stream->hFile,
			0,
			NULL,
			SEEK_CUR
		);
		if (stream->file_cursor == INVALID_SET_FILE_POINTER) {
			stream->error = SO_EOF;
			return -1;
		}

		/* Mutam file_pointer-ul la finalul fisierului */
		/* pentru a scrie in mod append */
		stream->file_cursor = SetFilePointer(
			stream->hFile,
			0,
			NULL,
			SEEK_END
		);
		if (stream->file_cursor == INVALID_SET_FILE_POINTER) {
			stream->error = SO_EOF;
			return -1;
		}
	}


	/* Scriem datele in fisier */
	bRet = WriteFile(
		stream->hFile,
		stream->buffer,
		stream->write_index,
		&written_bytes,
		NULL
	);
	if (bRet == FALSE) {
		stream->error = SO_EOF;
		return SO_EOF;
	}

	/* Setam buffer-ul pe 0 */
	memset(stream->buffer, 0, BUF_SIZE);
	/* Resetam cursorul bufferului pentru a scrie de la inceput */
	stream->write_index = 0;
	stream->read_index = -1;

	/* Daca eram in modul append, mutam file_cursor-ul */
	/* inapoi in pozitia initiala */
	if (stream->mode == MODE_APPEND || stream->mode == MODE_APPEND_PLUS) {
		/* Mutam file_pointer-ul la finalul fisierului */
		/* pentru a scrie in mod append */
		stream->file_cursor = SetFilePointer(
			stream->hFile,
			old_real_file_cursor,
			NULL,
			SEEK_SET
		);
		if (stream->file_cursor == INVALID_SET_FILE_POINTER) {
			stream->error = SO_EOF;
			return -1;
		}
	}

	return 0;
}

int so_fputc(int c, SO_FILE *stream)
{
	int rc = 0;

	if (stream->write_index == -1)
		stream->write_index = 0;

	/* In cazul in care bufferul este plin, facem fflush */
	if (stream->write_index == BUF_SIZE)
		rc = so_fflush(stream);

	/* Verificam daca nu am avut probleme in momentul scrierii datelor */
	if (rc == SO_EOF)
		return SO_EOF;

	/* Salvam caracterul in buffer */
	stream->buffer[stream->write_index] = (unsigned char) c;
	stream->file_cursor++;
	stream->write_index++;
	stream->last_op = WRITE_OP;

	return c;
}

size_t so_fread(void *ptr, size_t size, size_t nmemb, SO_FILE *stream)
{
	unsigned char *buffer;
	int chunk, index, c;

	/* Alocam structura in care vom citi datele */
	buffer = (unsigned char *) calloc(nmemb * size, sizeof(unsigned char));
	if (buffer == NULL) {
		stream->error = SO_EOF;
		return 0;
	}

	/* Citim datele */
	for (chunk = 0; chunk < nmemb; chunk++)
		for (index = 0; index < size; index++) {
			c = so_fgetc(stream);
			if (c != SO_EOF)
				buffer[chunk * size + index] =
					(unsigned char) c;
			else {
				/* Copiem datele citite la adresa ptr */
				memcpy(ptr, buffer, chunk * size + index);
				free(buffer);
				return chunk;
			}
		}

	/* Copiem datele citite la adresa ptr */
	memcpy(ptr, buffer, size * nmemb);
	free(buffer);

	return nmemb;
}

size_t so_fwrite(const void *ptr, size_t size, size_t nmemb, SO_FILE *stream)
{
	int chunk, index, c;

	unsigned char *text = (unsigned char *) ptr;

	for (chunk = 0; chunk < nmemb; chunk++)
		for (index = 0; index < size; index++) {
			c = so_fputc((int) text[chunk * size + index], stream);
			if (c == SO_EOF) {
				stream->error = SO_EOF;
				return chunk;
			}
		}

	return nmemb;
}

int so_fseek(SO_FILE *stream, long offset, int whence)
{
	int rc;

	/* Daca ultima operatie a fost una de scriere */
	/* atunci scriem datele din buffer */
	if (stream->last_op == WRITE_OP) {
		rc = so_fflush(stream);
		if (rc == -1) {
			stream->error = SO_EOF;
			return -1;
		}
	}

	/* Invalidam buffer-ul */
	memset(stream->buffer, 0, BUF_SIZE);
	stream->read_index = -1;
	stream->write_index = -1;
	stream->read_limit = -BUF_SIZE;
	stream->last_op = NO_OP;

	stream->file_cursor = SetFilePointer(
		stream->hFile,
		offset,
		NULL,
		whence
	);
	if (stream->file_cursor == INVALID_SET_FILE_POINTER) {
		stream->error = SO_EOF;
		return -1;
	}

	return 0;
}

long so_ftell(SO_FILE *stream)
{
	/* lseek va returna direct -1 sau pozitia curenta */
	return stream->file_cursor;
}

HANDLE so_fileno(SO_FILE *stream)
{
	return stream->hFile;
}

int so_feof(SO_FILE *stream)
{
	if (stream->read_limit == 0)
		return 1;
	return 0;
}

int so_ferror(SO_FILE *stream)
{
	return stream->error;
}

char **parse_command(const char *command)
{
	char **args;

	/* Vom executa o comanda de forma "sh" "-c" command */
	args = (char **) malloc(4 * sizeof(char *));
	args[0] = "sh";
	args[1] = "-c";
	args[2] = (char *) command;
	args[3] = NULL;

	return args;
}

SO_FILE *so_popen(const char *command, const char *type)
{
	SO_FILE *stream;
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	DWORD rc;
	BOOL bRes;
	CHAR cmdLine[1000];

	SECURITY_ATTRIBUTES sa;
	HANDLE hChildStdoutRead, hChildStdoutWrite;
	HANDLE hChildStdinRead, hChildStdinWrite;

	if (strcmp(type, "r") != 0 && strcmp(type, "w") != 0)
		return NULL;

	ZeroMemory(&sa, sizeof(sa));
	sa.bInheritHandle = TRUE;

	/* Curatam structurile astea */
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	si.dwFlags |= STARTF_USESTDHANDLES;
	ZeroMemory(&pi, sizeof(pi));

	/* Setam capetele de pipe */
	if (strcmp(type, "r") == 0) {
		bRes = CreatePipe(
			&hChildStdoutRead,
			&hChildStdoutWrite,
			&sa,
			0
		);
		if (bRes == FALSE)
			return NULL;
		si.hStdOutput = hChildStdoutWrite;

		bRes = SetHandleInformation(
			hChildStdoutRead,
			HANDLE_FLAG_INHERIT,
			0
		);
		if (bRes == FALSE)
			return NULL;
	} else {
		bRes = CreatePipe(
			&hChildStdinRead,
			&hChildStdinWrite,
			&sa,
			0
		);
		if (bRes == FALSE)
			return NULL;
		si.hStdInput = hChildStdinRead;
		bRes = SetHandleInformation(
			hChildStdinWrite,
			HANDLE_FLAG_INHERIT,
			0
		);
		if (bRes == FALSE)
			return NULL;
	}

	/* Ne formam comanda pe care trebuie sa o execute procesul */
	sprintf(cmdLine, "cmd /C %s", command);

	/* Pornim procesul. Hai tata, stiu ca poti! */
	bRes =  CreateProcess(
		NULL,
		cmdLine,
		NULL,
		NULL,
		TRUE,
		0,
		NULL,
		NULL,
		&si,
		&pi
	);
	if (bRes == FALSE)
		return NULL;

	/* Cream un nou SO_FILE in care vom avea ca file_descriptor */
	/* capatul de pipe pastrat deschis de catre proces */
	stream = malloc(sizeof(SO_FILE));
	if (stream == NULL)
		return NULL;
	stream->buffer = calloc(BUF_SIZE, sizeof(unsigned char));
	if (stream->buffer == NULL) {
		free(stream);
		return NULL;
	}
	stream->read_limit = -BUF_SIZE;
	stream->read_index = -1;
	stream->write_index = -1;
	stream->error = 0;
	stream->file_cursor = -1;
	stream->is_popen = 1;
	stream->last_op = NO_OP;
	stream->mode = parse_mode(type);
	stream->si = si;
	stream->pi = pi;
	/* Facem pipe-ul */
	if (strcmp(type, "r") == 0) {
		stream->hFile = hChildStdoutRead;
		CloseHandle(hChildStdoutWrite);
	} else {
		stream->hFile = hChildStdinWrite;
	}

	return stream;
}

int so_pclose(SO_FILE *stream)
{
	int rc;
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	DWORD dwRes;
	BOOL bRes;

	/* Inchidem capatul de pipe din care citeam */

	pi = stream->pi;
	si = stream->si;
	if (stream->mode == MODE_READ)
		CloseHandle(si.hStdOutput);
	else
		CloseHandle(si.hStdInput);

	rc = so_fclose(stream);
	if (rc == -1)
		return -1;

	/* Asteptam ca procesul sa se termine */
	/* Wait for the child to finish */
	dwRes = WaitForSingleObject(pi.hProcess, INFINITE);
	if (dwRes == WAIT_FAILED)
		return -1;

	bRes = GetExitCodeProcess(pi.hProcess, &dwRes);
	if (bRes == FALSE)
		return -1;

	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);

	return dwRes;
}
