build: main.obj
	link /nologo /dll /out:so_stdio.dll /implib:so_stdio.lib main.obj

main.obj:
	cl /D_CRT_SECURE_NO_DEPRECATE -W3 /nologo -I. /Fomain.obj /c main.c

clean:
	del so_stdio.lib so_stdio.dll main.obj so_stdio.exp
